package com.betworks.app

import android.app.Application
import android.util.Log
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.embedding.engine.FlutterEngineCache
import io.flutter.embedding.engine.dart.DartExecutor
import io.flutter.plugin.common.MethodChannel

class ShellApp : Application() {

    override fun onCreate() {
        super.onCreate()

        // Creating the FlutterEngine
        val engine = FlutterEngine(this.applicationContext)

        // Warm up the FlutterEngine
        engine.dartExecutor.executeDartEntrypoint(
            DartExecutor.DartEntrypoint.createDefault()
        )

        // Cache it
        FlutterEngineCache.getInstance().put(FLUTTER_ENGINE_ID, engine)
    }

    companion object {
        const val FLUTTER_ENGINE_ID = "flutter engine"
        const val CHANNEL = "com.betworks.flutter/channel"
    }
}