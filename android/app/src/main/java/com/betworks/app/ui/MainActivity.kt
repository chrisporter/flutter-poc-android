package com.betworks.app.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.betworks.app.R
import com.betworks.app.ShellApp
import com.betworks.app.databinding.MainActivityBinding
import io.flutter.embedding.android.FlutterFragment


class MainActivity : AppCompatActivity() {

    private lateinit var binding: MainActivityBinding

    private var flutterFragment: FlutterFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setSupportActionBar(binding.toolbar)

        if (savedInstanceState == null) {
            showNativeFragment()
        }

        binding.bottomNavView.setOnItemSelectedListener { item ->
            when (item.title) {
                "Flutter" -> {
                    showFlutterFragment()
                    true
                }
                else -> {
                    showNativeFragment()
                    true
                }
            }
        }
    }

    private fun showFlutterFragment() {
        flutterFragment = FlutterFragment
            .withCachedEngine(ShellApp.FLUTTER_ENGINE_ID)
            .build()

        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, flutterFragment!!)
            .commit()
    }

    private fun showNativeFragment() {
        val fragment = NativeFragment.newInstance()

        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .commit()
    }

    override fun onPostResume() {
        super.onPostResume()
        flutterFragment?.onPostResume()
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        flutterFragment?.onNewIntent(intent)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        flutterFragment?.onBackPressed()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        flutterFragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onUserLeaveHint() {
        flutterFragment?.onUserLeaveHint()
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        flutterFragment?.onTrimMemory(level)
    }
}