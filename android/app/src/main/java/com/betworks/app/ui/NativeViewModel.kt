package com.betworks.app.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.betworks.app.ShellApp
import io.flutter.embedding.engine.FlutterEngineCache
import io.flutter.plugin.common.MethodChannel

class NativeViewModel : ViewModel() {

    private val _counter = MutableLiveData(0)

    init {
        // Listening to getValue method call from Flutter
        FlutterEngineCache.getInstance().get(ShellApp.FLUTTER_ENGINE_ID)?.let { engine ->
            MethodChannel(
                engine.dartExecutor.binaryMessenger,
                ShellApp.CHANNEL
            ).setMethodCallHandler { call, result ->
                if (call.method == METHOD_GET_VALUE) {
                    val value = call.argument<Int>(KEY_COUNTER)
                    _counter.postValue(value)
                    result.success(true)
                }
            }
        }
    }

    fun getCounter(): LiveData<Int> = _counter

    companion object {
        private const val METHOD_GET_VALUE = "getValue"
        private const val KEY_COUNTER = "counter"
    }
}