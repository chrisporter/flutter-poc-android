package com.betworks.app.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.betworks.app.databinding.NativeFragmentBinding

class NativeFragment : Fragment() {

    private var _binding: NativeFragmentBinding? = null
    private val binding: NativeFragmentBinding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = NativeFragmentBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val vieWModel = ViewModelProvider(requireActivity()).get(NativeViewModel::class.java)
        vieWModel.getCounter().observe(viewLifecycleOwner, { value ->
            binding.counter.text = value.toString()
        })
    }

    companion object {
        fun newInstance() = NativeFragment()
    }
}